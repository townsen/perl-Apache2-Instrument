package Apache2::Instrument;

use strict;
use warnings;

=head1 NAME

Apache2-Instrument - An instrumentation framework for mod_perl

=head1 SYNOPSIS

Add one or more subclasses in your httpd.conf file:

 PerlInitHandler Apache2::Instrument::Time

=head2 OPTIONS

To activate instrumentation ONLY on a per request basis, add I<instrument>
to the end of your user agent, and add this line to httpd.conf:

 PerlSetVar Apache2-Instrument-Useragent 1

To log scoreboard information when the number of child processes
is at the maximum limit set a non-zero value for this variable in httpd.conf:

 PerlSetVar Apache2-Instrument-ScoreFreq 30

Note that this represents the minimum number of seconds between logged
scoreboard entries.
It also triggers automatic tracing when the number of servers is maxed out.
To limit the number of concurrent requests instrumented 
set the following variable in httpd.conf:

 PerlSetVar Apache2-Instrument-MaxTrace 3

=head1 DESCRIPTION

Five instrumentation handlers are available:

=over

=item C<Time>

Outputs the total request time.

=item C<Memory>

Outputs a GTop memory profile.

=item C<Strace>

Outputs a general purpose strace.

=item C<DBI>

Outputs a DBI::Profile dump.

=item C<Procview>

Outputs a combination strace and lsof report to a tempfile.

=back

See the source code for details.

=head1 AUTHOR

Phillipe M. Chiasson L<gozer@apache.org>

Version 0.03 released by Fred Moyer L<fred@redhotpenguin.com>

Version 0.04 released by Nick Townsend L<nick.townsend@mac.com>

Nick Townsend (github.com/townsen) contributed Procview (https://github.com/townsen/procview)

=head1 LICENCE AND COPYRIGHT

Copyright 2006 Phillipe M. Chiasson

This module is free software; you can redistribute it and/or
modify it under the same terms as Perl itself. See L<perlartistic>.

=cut

our $VERSION = '0.04';

use Apache::Scoreboard ();
use Apache2::Const -compile => qw(OK SERVER_ERROR);
use Apache2::Log ();
use Apache2::Process ();
use Apache2::RequestUtil ();
use Apache2::RequestRec ();
use Apache2::ServerRec ();
use Apache2::ServerUtil ();

use APR::Table ();

use Linux::Futex ();

use Data::Dumper;

use IPC::SysV qw(S_IROTH S_IWOTH S_IRUSR S_IWUSR IPC_CREAT);
use IPC::SharedMem;

Apache2::ServerUtil->server->push_handlers(PerlPostConfigHandler => \&post_config);
Apache2::ServerUtil->server->push_handlers(ChildInit => \&child_init);

use constant SHM_PERMS => S_IRUSR | S_IWUSR | S_IROTH | S_IWOTH;

# Configurable using PerlSetVar
sub MAX_TRACE {
    my $r = shift;
    return $r->dir_config('Apache2-Instrument-MaxTrace') // 0;
}
sub LOG_FREQ {
    my $r = shift;
    return $r->dir_config('Apache2-Instrument-LogFreq') // 0;
}

# $SHM is a handle to 12 bytes of shared memory, used as three 32 bit integers:
# 1. the Futex itself
# 2. the number of requests currently being traced
# 3. the time that the maximum number of children was logged
#
our $SHM;

# Be aware that logging goes to either main or vhost log depending on whether
# in Init handler (main) or cleanup handler (vhost)
sub debug {
    Apache2::ServerRec->log->debug($_[0]);
}

sub cleanup_instrument {
    debug("Cleaning up for server $$!!");
    if (defined $SHM) {
        $SHM->detach;
        $SHM->remove;
        $SHM = undef;
    }
}

sub setup_instrument {
    my $s = shift;
    debug("Enabling for server $$!!");
    Apache2::ServerUtil::server_shutdown_cleanup_register(\&cleanup_instrument);
    $SHM = IPC::SharedMem->new($$, 12, SHM_PERMS | IPC_CREAT);
    return Apache2::Const::SERVER_ERROR unless defined $SHM;
    $SHM->attach;
    Linux::Futex::init($SHM->addr);
    setvars(0,0);
    $s->log->alert(sprintf(__PACKAGE__." using shmid %d", $SHM->id));
    return Apache2::Const::OK;
}

sub post_config {
    my ($conf_pool, $log_pool, $temp_pool, $s) = @_;
    return (Apache2::ServerUtil::restart_count() > 1) ? setup_instrument($s): Apache2::Const::OK; 
}

sub child_init {
    my ($child_pool, $s) = @_;
    my $conf_pool = $s->process->pconf;
    $SHM->attach;
    debug(sprintf("Child $$ attached shmid %d",$SHM->id));
    return Apache2::Const::OK;
}

sub getvars {
    my ($count, $tstamp) = unpack("LL", $SHM->read(4, 8));
    debug("getvars = ($count, $tstamp)");
    return ($count, $tstamp);
}

sub setvars {
    my ($count, $tstamp) = @_;
    $SHM->write(pack("LL", $count, $tstamp), 4, 8);
    debug("setvars($count, $tstamp)");
}

sub notes {
    my ($class, $r, $v) = @_;
    if (defined $v) {
        return $r->pnotes($class, $v);
    }
    else {
        return $r->pnotes($class) || {};
    }
}

sub dumpScoreboard {

    my ($r) = @_;

    my $image = Apache::Scoreboard->image($r->pool);

    my %scoreboard_key = (
        '_' => 'Waiting for Connection',
        'S' =>  'Starting up',
        'R' => 'Reading Request',
        'W' => 'Sending Reply',
        'K' => 'Keepalive (read)',
        'D' => 'DNS Lookup',
        'C' =>  'Closing connection',
        'L' => 'Logging',
        'G' => 'Gracefully finishing',
        'I' => 'Idle cleanup of worker',
        '.' => 'Open slot with no current process'
    );

    my %worker_stats = map { $_ => 0 } keys %scoreboard_key;
    my @worker_scores;

    my $waiting_for_connections_count = 0;
    my $writing_response = 0;

    for (my $parent_score = $image->parent_score;
            $parent_score;
            $parent_score = $parent_score->next
        ) {

        my $pid = $parent_score->pid;

        my $worker_score = $parent_score->worker_score;
        push @worker_scores, {
            access_count => $worker_score->access_count,
            bytes_served => $worker_score->bytes_served,
            client => $worker_score->client,
            conn_bytes => $worker_score->conn_bytes,
            conn_count => $worker_score->conn_count,
            request => $worker_score->request,
            req_time => $worker_score->req_time,
            status => $worker_score->status,
            vhost=> $worker_score->vhost,
        };

        $worker_stats{$worker_score->status}++;
    }
    $Data::Dumper::Terse = 1;
    $Data::Dumper::Indent = 0;
    my $scores = Dumper(@worker_scores);
    $r->server->log->alert ("Scores: $scores");
    my $stats = Dumper(%worker_stats);
    $r->server->log->alert ("Stats: $stats");

}

sub lock {
    debug("Locking in $$");
    Linux::Futex::lock($SHM->addr);
    debug("Locked in $$");
}

sub unlock {
    debug("Unlocking in $$");
    Linux::Futex::unlock($SHM->addr);
    debug("Unlocked in $$");
}

sub handler : method {
    my ($class, $r) = @_;

    my $score_freq = $r->dir_config('Apache2-Instrument-ScoreFreq');
    $score_freq = 0 unless defined $score_freq;
    my $max_trace = $r->dir_config('Apache2-Instrument-MaxTrace');
    $max_trace = 0 unless defined $max_trace;
    my $instrument_request = 1;
    if ( $r->dir_config( 'Apache2-Instrument-Useragent' ) ) {
        my $ua = $r->headers_in->get( 'User-Agent' ) || 'notfound';
        $instrument_request = 0 if $ua !~ m/instrument$/i;
    }
    my $servers_left = 1;

    if ($score_freq > 0) {
        my $image = Apache::Scoreboard->image($r->pool);
        $servers_left = $image->server_limit - scalar(@{$image->pids});
        debug("Using ".scalar(@{$image->pids})." servers, limit is ".$image->server_limit);

        if ($servers_left == 0) {
            lock();
            my ($count, $fulltime) = getvars();
            my $now = time();
            if  ($now - $fulltime > $score_freq) {
                debug("All Servers used enabling instrumentation at $now");
                $fulltime = $now;
            }
            setvars($count, $fulltime);
            unlock();
            dumpScoreboard ($r) if $now == $fulltime;
        }
    }

    if ($instrument_request == 1 || ($score_freq > 0 && $servers_left == 0)) {
        lock();
        my ($count, $fulltime) = getvars();
        if (($max_trace == 0) || ($max_trace > 0 && $count < $max_trace)) {
            $r->push_handlers('CleanupHandler' => "${class}->cleanup" );
            my $notes = $r->pnotes($class) || {};
            $class->before($r, $notes);
            $r->pnotes($class, $notes);
            $count++;
            debug("Tracing request ".$r->the_request);
        }
        else {
            debug("Request not traced - maximum concurrent traces ($max_trace) reached");
        }
        setvars($count, $fulltime);
        unlock();
    }

    return Apache2::Const::OK;
}

sub cleanup : method {
    my ($class, $r) = @_;

    my $note = $r->pnotes($class) || {};

    $class->after($r, $note);

    my $req = $r->the_request;
    my $report = $class->report($r, $note);

    $Data::Dumper::Terse = 1;
    $Data::Dumper::Indent = 0;
    my $dump = Dumper($report);

    $r->log->alert("$class: $req: $dump");

    debug ("Cleanup $req");
    lock();
    my $lock_delay = $r->dir_config('Apache2-Instrument-LockDelay');
    if (defined($lock_delay) && $req =~ /sleep/) {
        debug("$$ sleeping holding lock");
        sleep($lock_delay);
        debug("$$ waking holding lock");
    }
    my ($count, $fulltime) = getvars();
    $count--;
    setvars($count, $fulltime);
    unlock();

    return Apache2::Const::OK;
}

1;
