Name:           perl-Apache2-Instrument
Version:        0.04
Release:        1%{?dist}
Summary:        Instrument Apache requests using mod_perl
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            https://github.com/townsen/%{name}
Source0:        https://github.com/townsen/%{name}/archive/%{version}.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)
BuildArch:      noarch
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(Exporter)
BuildRequires:  perl(Test)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(Linux::Futex)
Requires:       perl(:MODULE_COMPAT_%(eval "`%{__perl} -V:version`"; echo $version))
Requires:       mod_perl >= 2.0
Requires:       lsof >= 4.78
Requires:       strace >= 4.5.18
Requires:       perl(Linux::Futex)
Requires:       perl(IPC::SharedMem)
Requires:       perl(Apache::Scoreboard)

%{?perl_default_filter}

%description
Apache2::Instrument provides a framework for recording the execution
characteristics of web requests. It provides handler classes to 
measure: Execution Time, DBI activity, Memory usage and Strace.

%prep
%{__rm} -rf $RPM_BUILD_DIR/%{name}-%{version}
%{__tar} xzvf $RPM_SOURCE_DIR/%{name}-%{version}.tar.gz
%setup -T -D

%build
%{__perl} Makefile.PL INSTALLDIRS=vendor
%{__make} %{?_smp_mflags}

%install
%{__rm} -rf $RPM_BUILD_ROOT

%{__make} pure_install PERL_INSTALL_ROOT=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name .packlist -exec rm -f {} \;
find $RPM_BUILD_ROOT -depth -type d -exec rmdir {} 2>/dev/null \;

%{_fixperms} $RPM_BUILD_ROOT/*

%check
make test

%post

%{__chmod} u+s /usr/bin/strace /usr/sbin/lsof

%clean
%{__rm} -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc Changes
%doc /usr/share/man/man3/Apache2::Instrument.3pm.gz
%{perl_vendorlib}/*

%changelog
* Wed Jan 29 2014 Nick Townsend <nick.townsend@mac.com> - 0.04-1
- Added documentation
- Changed version back to 0.04 to fit CPAN

* Fri Jan 10 2014 Nick Townsend <nick.townsend@mac.com> - 0.12-2
- Added post install script to set strace and lsof setuid

* Wed Jan 08 2014 Nick Townsend <nick.townsend@mac.com> - 0.10
- Built for the first time for RHEL 6
