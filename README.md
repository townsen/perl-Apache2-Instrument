# Overview
This is an instrumentation framework for Perl under Apache2.
It was adapted from an original provided (though not authored) by Fred Moyer.

It consists of an Apache handler superclass that has four implementation classes:
    DBI - to profile accesses via the Database interface
    Time - to produce execution time statistics
    Memory - to show the memory usage
    Strace - to profile the system call activity
    Procview - to profile the I/O activity in detail

# Features
## Runtime toggling
To allow the handler to be present at all times on production machines, yet
only be triggered on a per-request basis (ie. when the need arises) the handler
inspects the User Agent string. If it ends with the word _instrument_ then the
request is analyzed.

This feature is enabled using the __Apache2-Instrument-Useragent__ directive.

# Procview Strace Instrumentation
This handler processes the request whilst running lsof and strace on the process.
It writes a temporary file containing the trace information to disk for later
analysis using the [http://github.com/townsen/procview](procview) gem.

## Pre-requisites

You'll need both lsof and strace from the following packages:
* lsof-4.82-4.el6.x86_64
* strace-4.5.19-1.17.el6.x86_64

They should be setuid root in order to get this information from other processes.

To analyze the traces install the 'procview' gem:

    gem install procview

You will also need to install:

* [Apache-Scoreboard](http://search.cpan.org/CPAN/authors/id/M/MJ/MJH/Apache-Scoreboard-2.09.2.tar.gz)
* [Linux-Futex](http://search.cpan.org/~phred/Linux-Futex-0.6/lib/Linux/Futex.pm)

## Installation
Use the traditional:

    perl Makefile.PL
    make
    make test
    make install

Alternatively you can use the provided RPM `.spec` file to build an RPM.

## Configuration
To use this enable in the httpd configuration file:

    ExtendedStatus On
    ScoreBoardFile '/tmp/apache_scoreboard.sb'

    PerlModule Apache2::Instrument

    <Location /perl/>
        SetHandler perl-script
        PerlInitHandler Apache2::Instrument::Procview
        PerlSetVar Apache2-Instrument-Useragent 1
        PerlSetVar Apache2-Instrument-MaxTrace 10
        PerlSetVar Apache2-Instrument-LogFreq  30
        PerlResponseHandler ModPerl::Registry
        PerlOptions +ParseHeaders
        Options +ExecCGI
        allow from all
    </Location>

Note the optional `PerlSetVar` directives, if not present they will default to:
* __Apache2-Instrument-Useragent__ will be 0 and disable the User Agent string trigger mechanism
* __Apache2-Instrument-MaxTrace__ will be 5 concurrent traces
* __Apache2-Instrument-LogFreq__ will be 30 seconds

# Usage

When an instrumentation event is triggered, the results are written to the Apache logfile at the _alert_ message level.
For the Procview code, the tracefiles are written to `/tmp` for analysis. Their names are `procview.<pid>.<seconds since epoch>.<microseconds>.trace`
